#include <DiagUtils.h>

tm Utils::GetUtcTime()
{
	time_t t = time(nullptr);
	tm tm;
	gmtime_s(&tm, &t);
	return tm;
}

std::string Utils::GetCurrentPath()
{
	char szPath[MAX_PATH] = {};
	GetCurrentDirectoryA(MAX_PATH, szPath);
	return szPath;
}

bool Utils::CreateSubDirectory(const std::string & path)
{
	std::string current;

	if (path.find('\\') != std::string::npos)
	{
		current = path.substr(0, path.find('\\'));
	}
	else
	{
		current = path;
	}

	do
	{
		if (CreateDirectoryA(current.c_str(), NULL) || GetLastError() == ERROR_ALREADY_EXISTS)
		{
			if (path.find('\\', current.length()) != std::string::npos)
			{
				current = path.substr(0, path.find('\\', current.length() + 1));
			}
			else
			{
				current = path;
			}
		}
		else
		{
			return false;
		}

	} while (current != path);

	return true;
}

std::vector<char> Utils::ReadFile(const std::string& filePath)
{
	FILE* fp = nullptr;
	fopen_s(&fp, filePath.c_str(), "rb");
	if (fp)
	{
		_fseeki64(fp, 0, SEEK_END);
		size_t iFileSize = (size_t)_ftelli64(fp);
		_fseeki64(fp, 0, SEEK_SET);

		std::vector<char> data(iFileSize);

		fread((void*)data.data(), 1, iFileSize, fp);
		fclose(fp);

		return data;
	}
	return {};
}

std::string Utils::GetFilename(const std::string & path, bool preserveExtension)
{
	size_t lastSlash = path.find_last_of('/');

	if (lastSlash == std::string::npos)
	{
		lastSlash = path.find_last_of('\\');
	}

	if (lastSlash != std::string::npos)
	{
		if (preserveExtension == false)
		{
			size_t ext = path.find_last_of('.');
			if (ext != std::string::npos && ext > lastSlash)
			{
				return path.substr(lastSlash + 1, ext - lastSlash - 1);
			}
		}

		return path.substr(lastSlash + 1);
	}

	if (preserveExtension == false)
	{
		size_t ext = path.find_last_of('.');
		if (ext != std::string::npos)
		{
			return path.substr(0, ext);
		}
	}

	return path;
}

std::string Utils::GetDirectory(const std::string & path)
{
	size_t lastSlash = path.find_last_of('/');

	if (lastSlash == std::string::npos)
	{
		lastSlash = path.find_last_of('\\');
	}

	if (lastSlash != std::string::npos)
	{
		return path.substr(0, lastSlash + 1);
	}

	return path;
}

std::string Utils::GetModulePath(HMODULE hModule)
{
	char buffer[MAX_PATH];
	GetModuleFileName(hModule, buffer, MAX_PATH);
	return buffer;
}

std::string Utils::GetModuleName(HMODULE hModule)
{
	std::string path = GetModulePath(hModule);
	return GetFilename(path);
}

std::wstring Utils::string2wstring(const std::string & str)
{
	int len = static_cast<int>(str.size());
	int wideLen = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), len, 0, 0);

	std::vector<wchar_t> wstr(wideLen);
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), len, wstr.data(), wideLen);

	return std::wstring(wstr.data(), wstr.data() + wideLen);
}

std::string Utils::wstring2string(const std::wstring & wstr)
{
	return std::string(wstr.begin(), wstr.end());
}

bool Utils::contains(const std::string & s, const std::string & find)
{
	return s.find(find) != std::string::npos;
}

std::vector<byte> Utils::GetRandomBytes(int len)
{
	std::vector<byte> bytes(len);
	std::random_device engine;

	for(int i = 0; i < len; i++)
	{
		bytes[i] = static_cast<byte>(engine() & 0xFF);
	}

	return bytes;
}

std::string Utils::BytesToHexString(const std::vector<byte>& bytes)
{
	std::stringstream ss;
	ss << std::hex << std::setfill('0');
	for (byte b : bytes)
	{
		ss << std::setw(2) << static_cast<int>(b);
	}
	return ss.str();
}
