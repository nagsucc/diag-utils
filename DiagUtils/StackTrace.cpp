#include <DiagUtils.h>

void StackTrace::Get(CONTEXT * ctx, std::ostream & output)
{
	output << "=== Stack Trace ===\n";
	// Based on https://stackoverflow.com/questions/22467604/how-can-you-use-capturestackbacktrace-to-capture-the-exception-stack-not-the-ca

	BOOL    result;
	HANDLE  process;
	HANDLE  thread;
	HMODULE hModule;

	STACKFRAME64        stack;
	ULONG               frame;
	DWORD64             displacement;

	DWORD disp;
	IMAGEHLP_LINE64 *line;

	char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
	char module[MaxNameLen];
	PSYMBOL_INFO pSymbol = (PSYMBOL_INFO)buffer;

	memset(&stack, 0, sizeof(STACKFRAME64));

	process = GetCurrentProcess();
	thread = GetCurrentThread();
	displacement = 0;
#if !defined(_M_AMD64)
	stack.AddrPC.Offset = (*ctx).Eip;
	stack.AddrPC.Mode = AddrModeFlat;
	stack.AddrStack.Offset = (*ctx).Esp;
	stack.AddrStack.Mode = AddrModeFlat;
	stack.AddrFrame.Offset = (*ctx).Ebp;
	stack.AddrFrame.Mode = AddrModeFlat;
#endif

	SymInitialize(process, NULL, TRUE); //load symbols

	for (frame = 0; ; frame++)
	{
		//get next call from stack
		result = StackWalk64
		(
#if defined(_M_AMD64)
			IMAGE_FILE_MACHINE_AMD64
#else
			IMAGE_FILE_MACHINE_I386
#endif
			,
			process,
			thread,
			&stack,
			ctx,
			NULL,
			SymFunctionTableAccess64,
			SymGetModuleBase64,
			NULL
		);

		if (!result) break;

		//get symbol name for address
		pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
		pSymbol->MaxNameLen = MAX_SYM_NAME;
		SymFromAddr(process, (ULONG64)stack.AddrPC.Offset, &displacement, pSymbol);

		line = (IMAGEHLP_LINE64 *)malloc(sizeof(IMAGEHLP_LINE64));
		line->SizeOfStruct = sizeof(IMAGEHLP_LINE64);

		if (SymGetLineFromAddr64(process, stack.AddrPC.Offset, &disp, line))
		{
			output
				<< "\tat " << pSymbol->Name
				<< " in " << line->FileName
				<< " : line: " << line->LineNumber
				<< ": address: 0x" << std::hex << line->Address << "\n";
		}
		else
		{
			output
				<< "\tat " << pSymbol->Name << ", address: 0x" << std::hex << line->Address;

			hModule = NULL;
			lstrcpyA(module, "");

			GetModuleHandleEx(
				GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
				(LPCTSTR)(stack.AddrPC.Offset), &hModule);

			if (hModule != NULL)
			{
				GetModuleFileNameA(hModule, module, MaxNameLen);
			}

			output << " in " << module << "\n";
		}

		free(line);
		line = NULL;
	}

	output << "=== End of Stack Trace ===\n";
}

bool StackTrace::WriteToFile(CONTEXT * ctx, const std::string & filename)
{
	std::fstream file(filename, std::ios::out | std::ios::trunc);

	if (!file.is_open())
	{
		return false;
	}

	Get(ctx, file);
	file.close();
	return true;
}
