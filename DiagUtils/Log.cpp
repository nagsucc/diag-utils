#include <DiagUtils.h>

std::mutex Log::mtx;
std::set<LogLevel> Log::logLevels = LOG_LEVEL;

LogBuffer Log::logBuffer;
NullBuffer Log::nullBuffer;

const std::string Log::LogLevelStrings[] =
{
	"ERROR",
	"WARNING",
	"INFO",
	"DEBUG",
	"TRACE"
};

void Log::SetLevels(const std::set<LogLevel>& logLevels)
{
	Log::logLevels = std::move(logLevels);
}

void Log::SetFilePath(const std::string& path)
{
	logBuffer.setOutputFile(path);
}

void Log::SetStream(std::ostream& stream)
{
	logBuffer.setOutputStream(stream);
}

std::ostream& Log::GetOutputStream(LogLevel logLevel, const std::string& filePath, int line)
{
	std::lock_guard<std::mutex> lock(mtx);

	static std::ostream null(&nullBuffer);
	static std::ostream log(&logBuffer);

	if (LogLevelEnabled(logLevel) == false)
	{
		return null;
	}

	log.clear();

	log << " [" << Timestamp::AsString()
		<< "] [" << LogLevelStrings[logLevel]
		<< "] (" << Utils::GetFilename(filePath)
		<< ":" << std::dec << line 
		<< ") ";

	return log;
}

std::istream& Log::GetInputStream()
{

	return logBuffer.getInternalStream();
}
