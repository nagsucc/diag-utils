#include <DiagUtils.h>

SysInfo::SysInfo()
{
	wmi.Initialize();
}

SysInfo::~SysInfo()
{
	wmi.Uninitialize();
}

void SysInfo::GetSystemReport(std::ostream & output)
{
	static const int col = 18;

	std::stringstream gpus;
	for (auto& gpu : GetGPUs())
	{
		gpus << '\n' << std::left << std::setw(col) << gpu.first << ": " << gpu.second;
	}

	std::stringstream cpus;
	for (auto& cpu : GetCPUs())
	{
		cpus << '\n' << std::left << std::setw(col) << cpu.first << ": " << cpu.second;
	}

	output << std::left
		<< "=== System Information ==="
		<< '\n' << std::setw(col) << "S/N" << ": " << GetSerialNumber()
		<< '\n' << std::setw(col) << "OS Name" << ": " << GetOSName()
		<< '\n' << std::setw(col) << "OS Version" << ": " << GetOSVersion()
		<< cpus.str()
		<< '\n' << std::setw(col) << "RAM" << ": " << (GetPhysicalMemorySize() / (1024 * 1024)) << " MB"
		<< gpus.str()
		<< "\n=== End of System Information ===\n";
}

std::string SysInfo::GetSerialNumber()
{
	std::string serial = "";
	IEnumWbemClassObject* pEnum = wmi.Query("SELECT * FROM Win32_BaseBoard");

	if (pEnum == nullptr)
	{
		return serial;
	}

	IWbemClassObject* pClassObject = nullptr;
	ULONG uRet = 0;
	HRESULT hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);

	if (uRet != 0)
	{
		VARIANT vtProp;

		hr = pClassObject->Get(L"SerialNumber", 0, &vtProp, 0, 0);
		if (SUCCEEDED(hr))
		{
			serial = Utils::wstring2string(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		pClassObject->Release();
	}

	pEnum->Release();
	return serial;
}

std::string SysInfo::GetOSName()
{
	std::string os = "";
	IEnumWbemClassObject* pEnum = wmi.Query("SELECT * FROM Win32_OperatingSystem");

	if (pEnum == nullptr)
	{
		return os;
	}

	IWbemClassObject* pClassObject = nullptr;
	ULONG uRet = 0;
	HRESULT hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);

	if (uRet != 0)
	{
		VARIANT vtProp;

		hr = pClassObject->Get(L"Caption", 0, &vtProp, 0, 0);
		if (SUCCEEDED(hr))
		{
			os += Utils::wstring2string(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		hr = pClassObject->Get(L"OSArchitecture", 0, &vtProp, 0, 0);
		if (SUCCEEDED(hr))
		{
			os += " (" + Utils::wstring2string(vtProp.bstrVal) + ")";
		}
		VariantClear(&vtProp);

		pClassObject->Release();
	}

	pEnum->Release();
	return os;
}

std::string SysInfo::GetOSVersion()
{
	std::string ver = "";
	IEnumWbemClassObject* pEnum = wmi.Query("SELECT * FROM Win32_OperatingSystem");

	if (pEnum == nullptr)
	{
		return ver;
	}

	IWbemClassObject* pClassObject = nullptr;
	ULONG uRet = 0;
	HRESULT hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);

	if (uRet != 0)
	{
		VARIANT vtProp;

		hr = pClassObject->Get(L"Version", 0, &vtProp, 0, 0);
		if (SUCCEEDED(hr))
		{
			ver = Utils::wstring2string(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		pClassObject->Release();
	}

	pEnum->Release();
	return ver;
}

std::map<std::string, std::string> SysInfo::GetGPUs()
{
	std::map<std::string, std::string> gpus;
	IEnumWbemClassObject* pEnum = wmi.Query("SELECT * FROM Win32_VideoController");

	if (pEnum == nullptr)
	{
		return gpus;
	}

	IWbemClassObject* pClassObject = nullptr;
	ULONG uRet = 0;
	HRESULT hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);

	while (hr == WBEM_S_NO_ERROR)
	{
		std::string gpuId;
		std::string gpuName;
		VARIANT vtProp;

		HRESULT res = pClassObject->Get(L"DeviceId", 0, &vtProp, 0, 0);
		if (SUCCEEDED(res))
		{
			gpuId = Utils::wstring2string(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		res = pClassObject->Get(L"Name", 0, &vtProp, 0, 0);
		if (SUCCEEDED(res))
		{
			gpuName = Utils::wstring2string(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		gpus[gpuId] = gpuName;

		pClassObject->Release();
		hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);
	}

	pEnum->Release();
	return gpus;
}

std::map<std::string, std::string> SysInfo::GetCPUs()
{
	std::map<std::string, std::string> cpus;
	IEnumWbemClassObject* pEnum = wmi.Query("SELECT * FROM Win32_Processor");

	if (pEnum == nullptr)
	{
		return cpus;
	}

	IWbemClassObject* pClassObject = nullptr;
	ULONG uRet = 0;
	HRESULT hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);

	while (hr == WBEM_S_NO_ERROR)
	{
		std::string cpuId;
		std::string cpuName;
		VARIANT vtProp;

		HRESULT res = pClassObject->Get(L"DeviceId", 0, &vtProp, 0, 0);
		if (SUCCEEDED(res))
		{
			cpuId = Utils::wstring2string(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		res = pClassObject->Get(L"Name", 0, &vtProp, 0, 0);
		if (SUCCEEDED(res))
		{
			cpuName = Utils::wstring2string(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		cpus[cpuId] = cpuName;

		pClassObject->Release();
		hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);
	}

	pEnum->Release();
	return cpus;
}

uint64_t SysInfo::GetPhysicalMemorySize()
{
	uint64_t capacity = 0;
	IEnumWbemClassObject* pEnum = wmi.Query("SELECT * FROM Win32_PhysicalMemory");

	if (pEnum == nullptr)
	{
		return capacity;
	}

	IWbemClassObject* pClassObject = nullptr;
	ULONG uRet = 0;
	HRESULT hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);

	while (hr == WBEM_S_NO_ERROR)
	{
		std::string gpuInfo;
		VARIANT vtProp;

		HRESULT res = pClassObject->Get(L"Capacity", 0, &vtProp, 0, 0);
		if (SUCCEEDED(res))
		{
			capacity += std::stoull(vtProp.bstrVal);
		}
		VariantClear(&vtProp);

		pClassObject->Release();
		hr = pEnum->Next(WBEM_INFINITE, 1, &pClassObject, &uRet);
	}

	pEnum->Release();
	return capacity;
}
