#include <DiagUtils.h>

std::string Timestamp::AsString()
{
	using namespace std::chrono;

	auto now = system_clock::now();
	time_t t = system_clock::to_time_t(now);

	const duration<double> ts = now.time_since_epoch();
	seconds::rep ms = duration_cast<milliseconds>(ts).count() % 1000;

	tm tm;
	gmtime_s(&tm, &t);

	char stamp[24] = {};

	sprintf_s(stamp,
		24,
		"%u-%02u-%02u %u:%02u:%02u.%03u",
		tm.tm_year + 1900,
		tm.tm_mon + 1,
		tm.tm_mday,
		tm.tm_hour + 1,
		tm.tm_min,
		tm.tm_sec,
		static_cast<uint16_t>(ms));

	return stamp;
}
