#include <DiagUtils.h>

HRESULT WMIConnector::Initialize()
{
	LOG_TRACE("Initializing...");

	// Initialize COM
	HRESULT hr = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hr))
	{
		LOG_ERROR("Failed to initialize COM! (" << std::hex << hr << ")");
		return hr;
	}

	// Initialize security
	hr = CoInitializeSecurity(
		nullptr,
		-1,
		nullptr,
		nullptr,
		RPC_C_AUTHN_LEVEL_DEFAULT,
		RPC_C_IMP_LEVEL_IMPERSONATE,
		nullptr,
		EOAC_NONE,
		NULL);

	if (FAILED(hr))
	{
		if (hr == RPC_E_TOO_LATE)
		{
			LOG_WARNING("Note: CoInitializeSecurity returned RPC_E_TOO_LATE");
		}
		else
		{
			LOG_ERROR("Failed to initialize security! (" << std::hex << hr << ")");
			Uninitialize();
			return hr;
		}
	}

	// Get locator to WMI
	hr = CoCreateInstance(CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER, IID_IWbemLocator, (LPVOID*)&pLoc);
	if (FAILED(hr))
	{
		LOG_ERROR("Failed to create IWbemLocator! (" << std::hex << hr << ")");
		Uninitialize();
		return hr;
	}

	// Connect to WMI
	hr = pLoc->ConnectServer(_bstr_t(L"ROOT\\CIMV2"), 0, 0, 0, 0, 0, 0, &pSvc);
	if (FAILED(hr))
	{
		LOG_ERROR("Could not connect to WMI! (" << std::hex << hr << ")");
		Uninitialize();
		return hr;
	}

	// Set proxy blanket
	hr = CoSetProxyBlanket(
		pSvc,
		RPC_C_AUTHN_WINNT,
		RPC_C_AUTHZ_NONE,
		nullptr,
		RPC_C_AUTHN_LEVEL_CALL,
		RPC_C_IMP_LEVEL_IMPERSONATE,
		nullptr,
		EOAC_NONE);

	if (FAILED(hr))
	{
		LOG_ERROR("Could not set proxy blanket. (" << std::hex << hr << ")");
		Uninitialize();
		return hr;
	}

	LOG_TRACE("Connected to WMI");
	return hr;
}

void WMIConnector::Uninitialize()
{

	LOG_TRACE("Uninitializing...");
	if (pSvc)
		pSvc->Release();

	if (pLoc)
		pLoc->Release();

	CoUninitialize();
	LOG_TRACE("Disconnected from WMI");
}

IEnumWbemClassObject * WMIConnector::Query(const char * query)
{
	if (pSvc == nullptr)
		return nullptr;

	IEnumWbemClassObject* pEnum = nullptr;

	HRESULT hr = pSvc->ExecQuery(
		bstr_t("WQL"),
		bstr_t(query),
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
		nullptr,
		&pEnum);

	if (FAILED(hr))
	{
		LOG_ERROR("Query failed! (" << std::hex << hr << ")");
		return nullptr;
	}

	return pEnum;
}
