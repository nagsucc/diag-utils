#include <DiagUtils.h>

const wchar_t* const Web::WebRequest::methods[] =
{
	L"GET", L"POST", L"PUT", L"DELETE"
};

Web::WebRequest::WebRequest(Method method, const std::string& url)
{
	this->method = method;
	SetUrl(url);
}

Web::WebRequest::~WebRequest()
{

}

bool Web::WebRequest::isReady() const
{
	return response.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

bool Web::WebRequest::isError() const
{
	return (this->error != NOERROR);
}

void Web::WebRequest::Send()
{
	this->response = std::async(std::launch::async, SendThreadMain, this);
}

void Web::WebRequest::Cancel()
{
	this->cancel = true;
}

bool Web::WebRequest::SetUrl(std::string url)
{
	try
	{
		ParseUrl(url);
	}
	catch (std::invalid_argument)
	{
		return false;
	}
	catch (std::out_of_range)
	{
		return false;
	}

	return true;
}

std::string Web::WebRequest::GetUrl() const
{
	return protocol + host + path;
}

void Web::WebRequest::SetData(const std::vector<char>& bytes)
{
	if (this->method == Web::Method::Get || this->method == Web::Method::Delete)
	{
		throw new std::runtime_error("Request body not allowed with Get/Delete");
	}

	this->data = std::move(bytes);
}

void Web::WebRequest::SetData(const std::string & data)
{
	if (this->method == Web::Method::Get || this->method == Web::Method::Delete)
	{
		throw new std::runtime_error("Request body not allowed with Get/Delete");
	}

	const char* bytes = data.data();
	this->data = std::vector<char>(bytes, bytes + data.length());
}

const std::vector<char>& Web::WebRequest::GetData() const
{
	return this->data;
}

void Web::WebRequest::AddHeader(const std::string& name, const std::string& value)
{
	this->headers[name] = value;
}

std::string Web::WebRequest::GetHeaders() const
{
	std::string header = "";

	for (const auto& h : headers)
	{
		header
			.append(h.first)
			.append(" : ")
			.append(h.second)
			.append("; ");
	}

	return header;
}

void Web::WebRequest::ParseUrl(std::string & url)
{	
	// Parse protocol
	size_t protocolEnd = url.find("://");
	if (protocolEnd != std::string::npos)
	{
		protocol = url.substr(0, protocolEnd);
		url = url.substr(protocolEnd + 3);
	}

	size_t portBegin = url.find(":");
	size_t pathBegin = url.find("/");

	size_t hostEnd = (portBegin != std::string::npos) ? portBegin : pathBegin;

	// Parse host
	if (hostEnd != std::string::npos)
	{
		this->host = url.substr(0, hostEnd);
	}
	else
	{
		this->host = url;
	}

	// Parse port
	if (portBegin != std::string::npos)
	{
		std::string sPort;

		if (pathBegin != std::string::npos)
		{
			sPort = url.substr(portBegin + 1, pathBegin - portBegin - 1);
		}
		else
		{
			sPort = url.substr(portBegin + 1);
		}

		uint16_t iPort = std::stoi(sPort);
		this->port = static_cast<uint16_t>(iPort);
	}

	// Parse path
	if (pathBegin != std::string::npos)
	{
		this->path = url.substr(pathBegin);
	}
	else
	{
		this->path = "";
	}

}

Web::WebResponse Web::WebRequest::SendThreadMain(Web::WebRequest *request)
{
	WebResponse response;
	HINTERNET hSession, hConnection, hRequest;

	// Connect
	hSession = WinHttpOpen(
		WEB_DEFAULT_USER_AGENT,
		WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
		WINHTTP_NO_PROXY_NAME,
		WINHTTP_NO_PROXY_BYPASS, 0);

	if (hSession == NULL)
	{
		request->error = GetLastError();
		return response;
	}

	std::wstring protocol = Utils::string2wstring(request->protocol);
	std::wstring host = Utils::string2wstring(request->host);
	std::wstring path = Utils::string2wstring(request->path);

	std::wstring url = protocol + L"://" + host;
	hConnection = WinHttpConnect(hSession, host.c_str(), INTERNET_DEFAULT_PORT, 0);

	if (hConnection == NULL)
	{
		request->error = GetLastError();
		return response;
	}

	hRequest = WinHttpOpenRequest(
		hConnection, methods[request->method],
		path.c_str(), NULL,
		WINHTTP_NO_REFERER, 
		WINHTTP_DEFAULT_ACCEPT_TYPES,
		WINHTTP_FLAG_SECURE);

	if (hRequest == NULL)
	{
		request->error = GetLastError();
		return response;
	}

	// Send
	std::wstring headers = Utils::string2wstring(request->GetHeaders());

	LPVOID pSendData = WINHTTP_NO_REQUEST_DATA;
	DWORD dataLen = 0;

	if (request->method == Web::Method::Post || request->method == Web::Method::Put)
	{
		pSendData = request->data.data();
		dataLen = static_cast<DWORD>(request->data.size());
	}

	BOOL bRes = WinHttpSendRequest(
		hRequest, 
		headers.c_str(), (DWORD) headers.size(), 
		pSendData, dataLen,
		(DWORD)headers.size() + dataLen, 0);

	if (bRes == FALSE)
	{
		request->error = GetLastError();
		return response;
	}

	// Receive
	bRes = WinHttpReceiveResponse(hRequest, NULL);

	if (bRes == FALSE)
	{
		request->error = GetLastError();
		return response;
	}

	// Status code
	DWORD dwStatusSize = sizeof(response.responseCode);
	WinHttpQueryHeaders(hRequest,
		WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER,
		WINHTTP_HEADER_NAME_BY_INDEX,
		&response.responseCode, &dwStatusSize, WINHTTP_NO_HEADER_INDEX);

	// Headers
	DWORD dwHeadersSize = 0;
	WinHttpQueryHeaders(hRequest,
		WINHTTP_QUERY_RAW_HEADERS_CRLF, WINHTTP_HEADER_NAME_BY_INDEX,
		NULL, &dwHeadersSize, WINHTTP_NO_HEADER_INDEX);

	if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
	{
		std::vector<wchar_t> headerData(dwHeadersSize + 1);

		bRes = WinHttpQueryHeaders(hRequest,
			WINHTTP_QUERY_RAW_HEADERS_CRLF, WINHTTP_HEADER_NAME_BY_INDEX,
			headerData.data(), &dwHeadersSize, WINHTTP_NO_HEADER_INDEX);

		std::stringstream responseHeaders(Utils::wstring2string(headerData.data()));
		std::string header;

		while (std::getline(responseHeaders, header) && header != "\r")
		{
			size_t i = header.find(':');

			if (i != std::string::npos)
			{
				response.responseHeaders[header.substr(0, i)] = header.substr(i + 1);
			}
		}

		auto it = response.responseHeaders.find("Content-Length");
		if (it != response.responseHeaders.end())
		{
			uint64_t size = std::stoull(it->second);
			request->totalBytes = size;
		}
	}

	// Download
	DWORD dwSize = 0;
	DWORD dwDownloaded = 0;

	response.responseBody.resize(request->totalBytes);

	do
	{
		dwSize = 0;
		
		if (WinHttpQueryDataAvailable(hRequest, &dwSize) == FALSE)
		{
			request->error = GetLastError();
			return response;
		}

		LPVOID pBuffer = response.responseBody.data() + request->downloadedBytes;

		if (WinHttpReadData(hRequest, pBuffer, dwSize, &dwDownloaded) == FALSE)
		{
			request->error = GetLastError();
			return response;
		}

		request->downloadedBytes += dwDownloaded;
	} 
	while (dwSize > 0 && !request->cancel);

	return response;
}
