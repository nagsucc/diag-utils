#include <DiagUtils.h>

LogBuffer::LogBuffer()
{
}

LogBuffer::~LogBuffer()
{
	if (pFileOutput && pFileOutput->is_open())
	{
		pFileOutput->close();
		pFileOutput.reset();
	}
}

int LogBuffer::sync()
{
	if (pFileOutput && pFileOutput->is_open())
	{
		*pFileOutput << this->str() << std::flush;
	}

	if (pOutputStream)
	{
		*pOutputStream << this->str();
	}

	if (bDebugOutput)
	{
		OutputDebugString(this->str().c_str());
	}

	if (bInternalStream)
	{
		internalStream << this->str();
	}

	this->str("");
	return 0;
}

void LogBuffer::setOutputStream(std::ostream & os)
{
	pOutputStream = &os;
}

void LogBuffer::setOutputFile(const std::string & out)
{
	if (pFileOutput && pFileOutput->is_open())
	{
		pFileOutput->close();
	}

	pFileOutput.reset(new std::fstream(out, std::ios::app));

	if (pFileOutput->is_open() == false)
	{
		pFileOutput.reset();
	}
}

void LogBuffer::enableDebugOutput(bool enabled)
{
	bDebugOutput = enabled;
}

void LogBuffer::enableInternalStream(bool enabled)
{
	bInternalStream = enabled;
}

std::istream & LogBuffer::getInternalStream()
{
	return internalStream;
}
