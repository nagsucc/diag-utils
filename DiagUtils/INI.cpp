#include <DiagUtils.h>

INI::INI()
{
	// Get default
	std::string module = Utils::GetModulePath();
	std::string path = Utils::GetDirectory(module);
	std::string filename = Utils::GetFilename(module, false);

	std::string config = path + filename + ".ini";
	if (Utils::FileExists(config))
	{
		configFile = config;
	}
}

INI::INI(const std::string & filePath)
{
	if (Utils::FileExists(filePath))
	{
		configFile = filePath;
	}
}

INI::~INI()
{
}

std::string INI::ReadString(const std::string & section, const std::string & key, const std::string& defaultValue)
{
	if (configFile.empty())
	{
		return defaultValue;
	}

	char buffer[512] = {};

	GetPrivateProfileString(
		section.c_str(), key.c_str(), defaultValue.c_str(), 
		buffer, sizeof(buffer), configFile.c_str());

	return buffer;
}

int INI::ReadInt(const std::string & section, const std::string & key, int defaultValue)
{
	if (configFile.empty())
	{
		return defaultValue;
	}
	return GetPrivateProfileInt(section.c_str(), key.c_str(), defaultValue, configFile.c_str());
}
