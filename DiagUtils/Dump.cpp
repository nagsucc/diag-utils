#include <DiagUtils.h>

bool Dump::Write(EXCEPTION_POINTERS* pExceptionPointers, const std::string& filepath, MINIDUMP_TYPE typeFlags)
{
	HANDLE hDumpFile = CreateFileA(filepath.c_str(), GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_WRITE | FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);

	MINIDUMP_EXCEPTION_INFORMATION ExceptionInformation;
	ExceptionInformation.ThreadId = GetCurrentThreadId();
	ExceptionInformation.ExceptionPointers = pExceptionPointers;
	ExceptionInformation.ClientPointers = TRUE;

	bool bSuccess = MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(),
		hDumpFile, typeFlags, &ExceptionInformation, NULL, NULL);

	CloseHandle(hDumpFile);

	return bSuccess;
}
