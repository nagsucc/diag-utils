#pragma once

class INI
{
public:

	INI();
	INI(const std::string& filePath);
	~INI();

	std::string ReadString(const std::string& section, const std::string& key, const std::string& defaultValue = "");
	int ReadInt(const std::string& section, const std::string& key, int defaultValue = 0);

private:

	std::string configFile;

};
