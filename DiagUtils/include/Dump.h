#pragma once

namespace Dump
{
	bool Write(
		EXCEPTION_POINTERS* pExceptionPointers,
		const std::string& filepath,
		MINIDUMP_TYPE typeFlags = MiniDumpNormal);
}
