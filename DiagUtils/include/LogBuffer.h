#pragma once

class LogBuffer : public std::stringbuf
{
public:

	LogBuffer();
	~LogBuffer();

	virtual int sync();

	void setOutputStream(std::ostream& os);
	void setOutputFile(const std::string& out);
	void enableDebugOutput(bool enabled);
	void enableInternalStream(bool enabled);

	std::istream& getInternalStream();

private:

	std::stringstream internalStream;

	std::unique_ptr<std::fstream> pFileOutput = nullptr;
	std::ostream* pOutputStream = nullptr;

	bool bDebugOutput = false;
	bool bInternalStream = true;

};
