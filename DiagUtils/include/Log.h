#pragma once
#include "LogBuffer.h"
#include "NullBuffer.h"

enum LogLevel
{
	Error,
	Warning,
	Info,
	Debug,
	Trace
};

#ifndef LOG_LEVEL
#if _DEBUG
#define LOG_LEVEL { Error, Warning, Info, Debug, Trace }
#else
#define LOG_LEVEL {  Error, Warning, Info }
#endif
#endif

class Log
{
public:

	static void SetLevels(const std::set<LogLevel>& logLevels);
	static void SetFilePath(const std::string& path);
	static void SetStream(std::ostream& stream);

	static std::ostream& GetOutputStream(LogLevel logLevel, const std::string& filePath, int line);
	static std::istream& GetInputStream();

private:

	static std::set<LogLevel> logLevels;
	static std::mutex mtx;

	static LogBuffer logBuffer;
	static NullBuffer nullBuffer;

	static inline bool LogLevelEnabled(LogLevel level)
	{
		return logLevels.find(level) != logLevels.end();
	}

	static const std::string LogLevelStrings[];
};

#define LOG_OUTPUT(level) Log::GetOutputStream(level, __FILE__, __LINE__)

#define LOG_OUTPUT_DEBUG LOG_OUTPUT(LogLevel::Debug)
#define LOG_OUTPUT_INFO LOG_OUTPUT(LogLevel::Info)
#define LOG_OUTPUT_WARNING LOG_OUTPUT(LogLevel::Warning)
#define LOG_OUTPUT_ERROR LOG_OUTPUT(LogLevel::Error)
#define LOG_OUTPUT_TRACE LOG_OUTPUT(LogLevel::Trace)

#define LOG_DEBUG(x) LOG_OUTPUT_DEBUG << x << std::endl
#define LOG_INFO(x) LOG_OUTPUT_INFO << x << std::endl
#define LOG_WARNING(x) LOG_OUTPUT_WARNING << x << std::endl
#define LOG_ERROR(x) LOG_OUTPUT_ERROR << x << std::endl
#define LOG_TRACE(x) LOG_OUTPUT_TRACE << x << std::endl
