#pragma once

class NullBuffer : public std::streambuf
{
public:
	int overflow(int c);
};
