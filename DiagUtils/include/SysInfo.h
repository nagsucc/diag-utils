#pragma once
#include "WMIConnector.h"

class SysInfo
{
public:

	SysInfo();
	~SysInfo();

	void GetSystemReport(std::ostream& output);

	std::string GetSerialNumber();
	std::string GetOSName();
	std::string GetOSVersion();
	std::map<std::string, std::string> GetGPUs();
	std::map<std::string, std::string> GetCPUs();
	uint64_t GetPhysicalMemorySize();

private:

	WMIConnector wmi;

};
