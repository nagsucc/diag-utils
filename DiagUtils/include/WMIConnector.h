#pragma once

class WMIConnector
{
public:

	HRESULT Initialize();
	void Uninitialize();

	IEnumWbemClassObject* Query(const char* query);

private:

	IWbemLocator* pLoc = nullptr;
	IWbemServices* pSvc = nullptr;

};
