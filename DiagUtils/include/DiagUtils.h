#pragma once

// Standard libraries
#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <ctime>
#include <sstream>
#include <mutex>
#include <vector>
#include <future>
#include <atomic>
#include <map>
#include <iomanip>
#include <set>
#include <algorithm>
#include <random>
#include <process.h>

// WINAPI
#ifndef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

// Dump.h/StackTrace.h dependencies
#include <DbgHelp.h>
#include <minidumpapiset.h>
#pragma comment (lib, "dbghelp.lib")

#ifdef _MSC_VER
#pragma comment(lib, "user32.lib")
#endif

// Web.h dependencies
#include <winhttp.h>
#pragma comment(lib, "Winhttp.lib")

// WMIConnector.h dependencies
#define _WIN32_DCOM
#include <comdef.h>
#include <Wbemidl.h>
#pragma comment(lib, "wbemuuid.lib")

// DiagUtils headers
#include "INI.h"
#include "Dump.h"
#include "Log.h"
#include "LogBuffer.h"
#include "NullBuffer.h"
#include "StackTrace.h"
#include "SysInfo.h"
#include "Timestamp.h"
#include "Utils.h"
#include "Web.h"
#include "WMIConnector.h"
