#pragma once

namespace Utils
{
	tm GetUtcTime();

	bool CreateSubDirectory(const std::string& path);
	std::vector<char> ReadFile(const std::string& filePath);

	std::string GetFilename(const std::string& path, bool preserveExtension = true);
	std::string GetDirectory(const std::string& path);
	std::string GetModulePath(HMODULE hModule = 0);
	std::string GetModuleName(HMODULE hModule = 0);
	std::string GetCurrentPath();

	inline bool FileExists(const std::string& path)
	{
		struct stat buffer;
		return (stat(path.c_str(), &buffer) == 0);
	}

	std::wstring string2wstring(const std::string& str);
	std::string wstring2string(const std::wstring& wstr);
	bool contains(const std::string& s, const std::string& find);

	std::vector<byte> GetRandomBytes(int len);
	std::string BytesToHexString(const std::vector<byte>& bytes);
}
