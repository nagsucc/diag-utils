#pragma once

#define WEB_DEFAULT_USER_AGENT L""

namespace Web
{
	class WebRequest;
	struct WebResponse;

	enum Method
	{
		Get,
		Post,
		Put,
		Delete
	};

	class WebRequest
	{
	public:

		WebRequest(Method method, const std::string& url);
		~WebRequest();

		bool isReady() const;
		bool isError() const;

		void Send();
		void Cancel();
		
		bool SetUrl(std::string url);
		std::string GetUrl() const;

		void SetData(const std::vector<char>& bytes);
		void SetData(const std::string& data);
		const std::vector<char>& GetData() const;

		void AddHeader(const std::string& name, const std::string& value);
		std::string GetHeaders() const;

		std::future<WebResponse> response;

		std::atomic<uint64_t> totalBytes = 0;
		std::atomic<uint64_t> downloadedBytes = 0;

	private:

		void ParseUrl(std::string& url);

		static WebResponse SendThreadMain(WebRequest* request);

		Method method;
		INTERNET_PORT port = INTERNET_DEFAULT_PORT;
		std::string protocol;
		std::string host;
		std::string path;

		std::map<std::string, std::string> headers;
		std::vector<char> data;

		std::atomic_bool cancel = false;
		std::atomic<DWORD> error = false;

		static const wchar_t* const methods[];
	};

	struct WebResponse
	{
		int responseCode = 0;
		std::map<std::string, std::string> responseHeaders;
		std::vector<char> responseBody;
	};
}
