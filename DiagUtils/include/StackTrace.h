#pragma once

namespace StackTrace
{
	const int MaxNameLen = 256;
	void Get(CONTEXT* ctx, std::ostream& output = std::cout);
	bool WriteToFile(CONTEXT* ctx, const std::string& filename);
}
